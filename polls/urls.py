from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from .apiviews import PollViewSet, ChoiceListCreate, CreateVote, UserListCreate, LoginView #UserViewSet

router = DefaultRouter()
router.register('polls', PollViewSet, base_name='polls')
# router.register('users', UserViewSet, base_name='users')

urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path("users/", UserListCreate.as_view(), name="user_list_create"),
    path("polls/<int:pk>/choices/", ChoiceListCreate.as_view(), name="choice_list_create"),
    path("polls/<int:pk>/choices/<int:choice_pk>/vote/", CreateVote.as_view(), name="create_vote"),
]

urlpatterns += router.urls